In case you want to read bash variables from python, here is an example for you. The method is not safe, it will execute the shell script, so you have to trust the input! But the nice part is, that even calculated variables are returned.

The key is, to append a set to the shell file and execute it and compare the output to an empty shell script with a set in it.

Here is an example with string concatination:

```
$ cat input/string_concatenation.conf
foo=bar
foo+=' baba'
$ python read-shell-vars.py input/string_concatenation.conf
{'foo': 'bar baba'}
```

And one with a list concatination:

```
$ cat input/list_concatenation.conf
lista=(a b c)
listb=(c d e)
listc=("${lista[@]}" "${listb[@]}")
$ python read-shell-vars.py input/list_concatenation.conf
{'lista': ['a', 'b', 'c'],
 'listb': ['c', 'd', 'e'],
 'listc': ['a', 'b', 'c', 'c', 'd', 'e']}
```
