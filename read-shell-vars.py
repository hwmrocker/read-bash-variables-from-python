from __future__ import unicode_literals
import sys
import re
from pprint import pprint
from subprocess import check_output


def check_output_shell(cmd):
    return check_output(["bash", "-c", cmd])


def _get_dict_tuples_for_list(var, value):
    return var, list(re.findall(r'\[\d+\]="([^"]*)"', value))


def _get_dict_tuples_for_string(var, value):
    if value[0] == "'":
        return var, value[1:-1]
    return var, value


def _get_dict_tuples(line):
    var, value = line.split('=', 1)
    if not value:
        return var, ""
    if value[0] == '(':
        return _get_dict_tuples_for_list(var, value)
    return _get_dict_tuples_for_string(var, value)


def load_config(config_filename):
    # to get the same set of env variables, we need to execute also multiple statements in one line
    default_env = check_output_shell("true;set").decode('utf8')

    config_data_list = open(config_filename).read().splitlines()
    config_data_list.append("set")
    # we join the lines with ; so the the BASH_EXECUTION_STRING will not contain newlines
    config_env_list = check_output_shell(";".join(config_data_list)).decode('utf8').splitlines()
    return dict(_get_dict_tuples(l) for l in config_env_list
                if l not in default_env and not l.startswith('BASH_EXECUTION_STRING'))


if __name__ == "__main__":
    pprint(load_config(sys.argv[1]))
